package brci0062MV.control;

import brci0062MV.model.Carte;

import org.junit.After;
import org.junit.Before;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



public class BibliotecaCtrlTestBBT {
    Carte carte;
    BibliotecaCtrl ctrl;


    private List<String> autoriCarte=new ArrayList<>(
            Arrays.asList("Rebreanu","Creanga"));

    private List<String> cuvinteCheie=new ArrayList<>(
            Arrays.asList("broscuta","lup"));


    @Before
    public void setUp() throws Exception {
        carte=new Carte();
        ctrl=new BibliotecaCtrl();

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test(expected = Exception.class)
    //date valide
    public void adaugaCarteLugimeTitluValid() throws Exception {
        carte=new Carte("Povesti",autoriCarte,"1900","Litera",cuvinteCheie);

        ctrl.adaugaCarte(carte);


    }
    @Test(expected = Exception.class)
    //date valide
    public void adaugaCarteAnAparitieValid() throws Exception {
        carte=new Carte("Povesti",autoriCarte,"1950","Litera",cuvinteCheie);
//
        ctrl.adaugaCarte(carte);


    }

    @Test(expected = Exception.class)
    //date valide
    public void adaugaCarteLungimeEdituraValid() throws Exception {
        carte=new Carte("Povesti",autoriCarte,"1900","Litera",cuvinteCheie);
//        ctrl=new BibliotecaCtrl();
//        assertEquals(6,c10.getEditura().length());
//        assertEquals("Povesti",c10.getTitlu());
//        assertEquals("1900",c10.getAnAparitie());
        ctrl.adaugaCarte(carte);
    }

    @Test(expected = Exception.class)
    //date valide
    public void adaugaCarteAnValid() throws Exception {
        carte=new Carte("Povesti",autoriCarte,"2018","Litera",cuvinteCheie);
        // assertEquals( 2018,Integer.parseInt(c10.getAnAparitie()));
        ctrl.adaugaCarte(carte);
    }


    @Test(expected = Exception.class)
    //date invalide
    public void adaugaCarteAnInvalid() throws Exception {
        carte=new Carte("Povesti",autoriCarte,"1700","Litera",cuvinteCheie);
        ctrl.adaugaCarte(carte);
    }

    @Test(expected = Exception.class)
    //date invalide
    public void adaugaCarteAnInvalidBVA() throws Exception {
        carte=new Carte("Povesti",autoriCarte,"2019","Litera",cuvinteCheie);
        ctrl.adaugaCarte(carte);
    }

    @Test(expected = Exception.class)
    //date invalide
    public void adaugaCarteEdituraInvalida() throws Exception {
        carte=new Carte("Povesti",autoriCarte,"1900","Lit",cuvinteCheie);
        ctrl.adaugaCarte(carte);

    }
    @Test(expected = Exception.class)
    //date invalide
    public void adaugaCarteTiluInvalid() throws Exception{
        carte=new Carte("Povvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv",autoriCarte,"1900","Lit",cuvinteCheie);
        ctrl.adaugaCarte(carte);

    }





}