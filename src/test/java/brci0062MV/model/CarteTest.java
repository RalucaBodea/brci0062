package brci0062MV.model;

import org.junit.*;

import static org.junit.Assert.*;

public class CarteTest {
    Carte c;
    Carte c1;
    Carte carte;


    //se apeleaza o sg data inaintea oricarui test
    @BeforeClass
    public static void setup(){
        System.out.println("Before any test");

    }

    @AfterClass
    public static void setupAfter(){
        System.out.println("After all tests");

    }
    //se apeleaza doar inaintea testului asupra caruia ii este adnotat
    @Before
    public void setUp() throws Exception {
        c = new Carte();
        carte = new Carte();
        carte.setEditura("Corint");
        c.setTitlu("povesti");
        c.setEditura("Litera");
        System.out.println("in BeforeTest");
    }

    @After
    public void tearDown() throws Exception {
        c = null;
        System.out.println("in AfterTest");

    }

    @Ignore //ignora testul asupra caruia este pus
    @Test
    public void getTitlu() {
        assertEquals("titlu = povesti", "povesti",c.getTitlu());
        // assertTrue("povesti", c.getTitlu());
    }
    @Test
    public void setTitlu() {
        c.setTitlu("fabule");
        assertEquals("fabule",c.getTitlu());
        // assertTrue("povesti", c.getTitlu());
    }

    //aici testul trece daca mi se arunca exceptia. trb sa simulam un comportament care sa arunce exceptia
    //daca pun an mai mic ca 1900 testu trece pt ca imi merge in exceptie (adica are de aruncat exceptie)
    @Test(expected = Exception.class)
    public void SetAnAparitie() throws Exception {
        c.setAnAparitie("1899");
    }

    @Test
    public void getEdit() throws Exception{
        assertEquals("Corint",carte.getEditura());

    }
    @Test(timeout = 10)
    public void getEditura(){
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
            assertEquals(c.getEditura(), "Litera");
        }

    }
    @Test(expected = NullPointerException.class)
    //arunca exceptie daca lista mea e diferita de null
    public void testCuvantCheie() {
        if (carte.getCuvinteCheie() != null)
            throw new NullPointerException();
    }

    @Test
    public void deleteCuvantCheie() {
        carte.deleteCuvantCheie("enigma");
    }

    @Test
    public void adaugaAutor()  {
            try {
                carte.adaugaAutor("Mircea");
                carte.adaugaAutor("Vasile");
                carte.adaugaAutor("Mircea");
                //aici imi verifica size ul listei de autori...daca pun 3 trece daca pun 2 trece
                assertEquals(2,carte.getAutori().size());
              // fail("Nu s-a ridicat exceptie pentru autor duplicat!");
            } catch (IllegalArgumentException iae) {
                iae.printStackTrace();
//                assertEquals("Eliade", carte.getAutori().size());
            }
//        CartiRepo repo = new CartiRepo();
//        repo.adaugaCarte(new Carte());
//        assertNotNull(repo.cautaCarte("Eminescu"));
//        assertEquals(1, repo.getCarti().size());
    }

}
