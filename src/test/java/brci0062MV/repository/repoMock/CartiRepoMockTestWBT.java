package brci0062MV.repository.repoMock;

import brci0062MV.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CartiRepoMockTestWBT {
    Carte carte1;
    Carte carte2;
    Carte carte3;
    Carte carte4;

    static CartiRepoMock cartiRepoMock;
    static List<Carte> carti;
    static List<Carte> cartiGasite;

    @BeforeClass
    public static void setup(){
        cartiRepoMock = new CartiRepoMock();
        carti = new ArrayList<Carte>();
        cartiGasite = new ArrayList<Carte>();
    }
    @Before
    public void setUp() throws Exception {
        List<String> autoriCarte1 = new ArrayList<>(Arrays.asList("abc"));
        List<String> autoriCarte2 = new ArrayList<>(Arrays.asList("Popescu"));
        List<String> autoriCarte3 = new ArrayList<>(Arrays.asList("Popescu Cioran"));
        List<String> autoriCarte4 = new ArrayList<>(Arrays.asList("Caragiale"));

        List<String> cuvinteCheieC1 = new ArrayList<>(Arrays.asList("Creanga", "ion"));
        List<String> cuvinteCheieC2 = new ArrayList<>(Arrays.asList("Mircea", "povesti"));
        List<String> cuvinteCheieC3 = new ArrayList<>(Arrays.asList("emil", "emil2"));
        List<String> cuvinteCheieC4 = new ArrayList<>(Arrays.asList("scrisoare", "ion"));

        carte1 = new Carte("Povesti",autoriCarte1,"1922","Corint",cuvinteCheieC1);
        carte2 = new Carte("Jurnal",autoriCarte2,"1900","Teora",cuvinteCheieC2);
        carte3 = new Carte("Iarna ",autoriCarte3,"18990","Litera ",cuvinteCheieC3);
        carte4 = new Carte("O scrisoare pierduta",autoriCarte4,"1912","Corint",cuvinteCheieC4);
    }

    @After
    public void tearDown() throws Exception {
        carte1 = null;
        carte2 = null;
        carte3 = null;
        carte4 = null;
        System.out.println("in AfterTest");
    }


    @Test
    public void cautaCarteListaNull() {
        List<Carte> lista = cartiRepoMock.cautaCarte("Tudor Arghezi");
        assert lista.size() == 0;
    }

    @Test
    public void testCautaCarteListaNenula(){
        cartiRepoMock.adaugaCarte(carte1);
        System.out.println(cartiRepoMock.getCarti());
        List<Carte> lista = cartiRepoMock.cautaCarte("abc");
        System.out.println(lista);
        assert lista.size() == 1;
    }

    @Test
    public void testCautaCarteListaMaiLunga(){
        cartiRepoMock.adaugaCarte(carte2);
        cartiRepoMock.adaugaCarte(carte3);
        cartiRepoMock.adaugaCarte(carte4);
        List<Carte> list = cartiRepoMock.cautaCarte("Popescu");
        assert list.size() == 2;
    }
}